class Airplane:
    def __init__(self, brand, total_number_of_seats):
        self.brand = brand
        self.total_distance_flown = 0
        self.total_number_of_seats = total_number_of_seats
        self.taken_seats = 0

    def fly_distance(self, distance):
        self.total_distance_flown += distance

    def is_service_required(self):
        return True if self.total_distance_flown > 10000 else False

    def board_passengers(self, number_of_passenger):
        if self.taken_seats + number_of_passenger > self.total_number_of_seats:
            self.taken_seats = self.total_number_of_seats
        else:
            self.taken_seats += number_of_passenger

    def get_available_seats(self):
        return self.total_number_of_seats - self.taken_seats


if __name__ == '__main__':
    boeing747 = Airplane('Boeing', 350)
    airbus = Airplane('Airbus', 210)
    airbus.fly_distance(9999)
    airbus.is_service_required()

    airbus.board_passengers(200)
    airbus.board_passengers(9)
    print(airbus.taken_seats)
    print(airbus.get_available_seats())

